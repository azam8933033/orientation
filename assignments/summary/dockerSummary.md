# Docker

**Why need a docker?**

Docker is used to deploying our projects in various enviroments.
![Docker](https://i2.wp.com/maximorlov.com/wp-content/uploads/2020/02/why-docker.png?w=1200&ssl=1)
## Different terminologies used in docker

* Docker      : For develop and run applications with containers.
* Docker Image: Contains coderuntim libraries environment variables configuration files
* Container   : A container is a running Docker image.
* Docker Hub  : Docker Hub is like GitHub but for docker images and containers.
![](https://miro.medium.com/max/700/1*9uNw2h81iTCEKzy6kvNRKQ.png)
## Installation Process

* Install Docker: sudo apt-get install docker-engine -y
* Start Docker  : sudo service docker start
* Verify Docker : sudo docker run hello-world

## Basic Commands 

*  **_docker ps_**    : It allows us to view all the containers that are running on the Docker Host.
* **_docker start_**  : This command starts any stopped container(s).
* **_docker stop_**   : This command stops any running container(s).
* **_docker run_**    : This command creates containers from docker images.
* **_docker rm_**     : This command deletes the containers
![basic flow](https://geekflare.com/wp-content/uploads/2019/09/docker-architecture.png)
