# Git

**What is Git?**

Git is a distributed _version-control_ system for tracking changes in source code during software development.
![git](https://miro.medium.com/max/700/1*l9IZ_LeUCP_vwxjDKI2Xgw.jpeg)
## Repository

A Git repository stores the entire revision history and the authors concerned of all the files of a project.

## Commit

Every logical change in git is considered a **_commit_**. A commit is a set of changes across multiple files and represents a logical addition/change to the project.

## Git Workflow



![workflow 1](https://nexuslinkservices.com/wp-content/uploads/2020/01/Git.jpg)
![workflow 2](https://nexuslinkservices.com/wp-content/uploads/2020/01/git-flow.jpg)

   * A develop branch is created from master.
   * A release branch is created from develop.
   * Feature branches are created from develop.
   * When a feature is complete it is merged into the develop branch.
   * When the release branch is done it is merged into develop and master.
   * If an issue in master is detected a hotfix branch is created from master.
   * Once the hotfix is complete it is merged to both develop and master

## Some Basic Commands

 - `git init` : Initializes an empty git repository in the curernt directory.
 - `git clone` : Creates a local copy of git repository that exists remotely.
 - `git add` : Stages a change i.e. adds a change to the staging area.
 - `git commit` : Adds the set of changes currently in the staging area to the repository, i.e "commits" the changes to the repository. A commit also includes a commit message and Author's Name and Email ID.
 - `git status` : shows the status of the repository, i.e. list of new files, modified files, staged files etc.
 - `git log` : shows a list of recent commits with author and commit messages.
 - `git branch` : creates a new branch in the repository which can worked on independent of the master copy/branch.
 - `git merge` : merges a branch to current branch thus incorporating all its new changes in current branch.
 - `git pull` : brings new changes in a remote repository to the local repository.
 - `git push` : sends the changes made locally to the remote repository.
